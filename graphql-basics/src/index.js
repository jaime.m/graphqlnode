import { assign } from 'apollo-utilities'

import { argsToArgsConfig } from 'graphql/type/definition';
import { NoUnusedFragments } from 'graphql/validation/rules/NoUnusedFragments';
import { GraphQLServer } from 'graphql-yoga'
import db from './db'
import Query from './resolvers/Query'
import User from './resolvers/User'
import Coment from './resolvers/Comment'
import Post from './resolvers/Post'
import Mutation from './resolvers/Mutation'

const server = new GraphQLServer
({
    typeDefs:'./src/schema.graphql',
    resolvers:{
        Query,
        Mutation,
        User,
        Post,
        Coment
    },
    context:{
        db:db
    }
})

server.start(() => {
    console.log('the graphql server is UP!')
})