let users=[{
    id:'1',
    name:'Jaime',
    email:'j@j.com',
    age:1
},{
    id:'2',
    name:'Karen',
    email:'k@k.com',
    age:1
},{
    id:'3',
    name:'c',
    email:'c@c.com',
    age:1
}
]

let posteos=[{
    id:'1',
    title:'qwe',
    body:'j@j.com',
    published:true,
    author:"1"
},{
    id:'2',
    title:'asd',
    body:'k@k.com',
    published:true,
    author:"1"
},{
    id:'3',
    title:'zxc',
    body:'l@l.com',
    published:false,
    author:"2"
}
]

let comentariesb=[{
    id:'1',
    text:'qwe',
    author:"2",
    post:'1'
},{
    id:'2',
    text:'qwe2',
    author:"1",
    post:'3'
},{
    id:'3',
    text:'qwe3',
    author:"1",
    post:'3'
}
,{
    id:'4',
    text:'qwe4',
    author:"2",
    post:'2'
}
]

const db ={
    users,
    posteos,
    comentariesb
}

export {db as default}