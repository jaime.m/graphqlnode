import uuidv4 from 'uuid/v4'

const Mutation={
    createUser(parent,args,{ db },info){
        //false none of the user have that email
        const emailTaken= db.users.some((user)=>
            user.email===args.data.email
        )
        if(emailTaken){
            throw new Error('Email taken.')
        }

        const user ={
            id:uuidv4(),
            ...args.data
        }

        db.users.push(user)
        console.log(user)

        return user
    },
    deleteUser(parent,args,{ db },info){        
        const userIndex = db.users.findIndex((user)=>
            user.id===args.id
        )

        if(userIndex===-1){
            throw new Error('User not found.')
        }
        
        let deleted = users.splice(userIndex,1)

        const posts= db.posteos.filter((post)=>{
            const match = post.author ===args.id;

            if(match){
                comments = db.comments.filter((Comment)=>{
                    return comment.post !=post.id
                })
            }
        })

        return deleted[0]
    },
    createPost(parent,args,{ db },info){
        
        const userExists= db.users.some((user)=>
            user.id===args.data.author                
    )
    if(!userExists){
        throw new Error('There s not user.')
    }

    const post ={
        id:uuidv4(),
        ...args.data
    }

    db.posteos.push(post)

    return post
    },
    createComment(parent,args,{ db },info){
        
        const userExists= db.users.some((user)=>
            user.id===args.data.author                
    )

    const postExists= db.posteos.some((post)=>
    post.id===args.data.post && post.published               
)


    if(!userExists&&postExists){
        throw new Error('There s not user or post.')
    }

    const commentarie ={
        id:uuidv4(),
        ...args.data
    }

    db.comentariesb.push(commentarie)

    return commentarie
    }

}

export { Mutation as default}