"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var prisma_lib_1 = require("prisma-client-lib");
var typeDefs = require("./prisma-schema").typeDefs;

var models = [
  {
    name: "Archetype",
    embedded: false
  },
  {
    name: "ArchetypeVariable",
    embedded: false
  },
  {
    name: "Company",
    embedded: false
  },
  {
    name: "CompanyArchtype",
    embedded: false
  },
  {
    name: "Employee",
    embedded: false
  },
  {
    name: "EmployeeAnswer",
    embedded: false
  },
  {
    name: "EmployeeSurvey",
    embedded: false
  },
  {
    name: "Option",
    embedded: false
  },
  {
    name: "Question",
    embedded: false
  },
  {
    name: "QuestionType",
    embedded: false
  },
  {
    name: "Role",
    embedded: false
  },
  {
    name: "Section",
    embedded: false
  },
  {
    name: "Survey",
    embedded: false
  },
  {
    name: "User",
    embedded: false
  },
  {
    name: "Variable",
    embedded: false
  }
];
exports.Prisma = prisma_lib_1.makePrismaClientClass({
  typeDefs,
  models,
  endpoint: `http://localhost:4466`
});
exports.prisma = new exports.Prisma();
